## 反馈和复习

| 姓名 | 意见或建议                                                   |
| ---- | ------------------------------------------------------------ |
| ***  | 关于自定义指令解决图片异常问题很懵，不太明白。               |
| ***  | token被动介入代码： if (error.response && error.response.data && error.response.data.code === 10002) { store.dispatch('user/logout') // 登出action 删除token router.push('/login') } else { Message.error(error.message) // 提示错误信息 } 可不可以去掉里面的else，这样跳转的时候就会提示错误信息，或者if里面加一个token过期的提示，会不会逻辑更严谨一些 |
| ***  | token失效的被动处理,看不到token失效返回的东西,手动修改了cookie的token才发现返回的数据具体有什么 |
| ***  | 存储token信息为啥用cookie而不是h5本地存储，是有什么优点吗，之前讲的是cookie有缺点才用的h5本地存储 |
| ***  | 对通过自定义指令来解决图片加载失败的问题和token失效的处理方法还有点不是很理解，希望老师在回顾的时候可以重点说一下这两处 |

 

> 自定义指令解决图片异常问题

图片为什么异常？

>   有地址，但是地址不是图片，响应回来是404 

怎么解决 ？

```html
<img id="img" src="sss" />
<script>
  document.getElementById('img').onerror = function () {
      // onerror 是该图片有地址 但是地址加载失败的时候 执行
  }
</script>
```

用自定义指令

>  为什么要自定义指令呢 ？  因为后期有可能别的图片也会有这种问题 

自定义指令可以通用 ，只写一次，后面都可以直接应用

```js
Vue.directive('指令名称', {})
```

```js
export const imagerror = {
    // 这是当前元素插入文档之后触发的函数
    inserted (dom,options) {
        dom.onerror = function () {
            dom.src = options.value
        }
    }
}
<img v-imagerror="defaultImg" />
```

```js
export default  A
import A  from '地址'
// 此时 A表示默认导出的对象
```

```js
export const A = {}
export function A () {
    
}
import { A }  from '地址'
```

```js
import * as A from '地址'
// A表示模块里面所有导出的内容 包含默认导出 和其他导出
```

```js
Object.keys(A).forEach(key => {
    // key是指令名称
    // value是 指令对象
    Vue.directive(key, A[key])
})
```

>    token的超时 主动处理/ 被动处理

主动处理前端自己做 => 因为token有时效性，但是 后端并没有告诉我什么时候过期，如果我传一个过期token ，它没有任何响应 =》  登录成功  =》 获取一个新的token =>  开始计时 （存入登录成功时的时间戳）

一旦我用到token向后端发请求的时候，就要去检查这个token有没有作用，检查它的时效性是否过期

拿当前的时间  - 登录成功时的时间戳  > 最大时间差  =》 认为超时，

```js
if (超时) {
    // 删除token 
    store.dispatch('user/logout')
    router.push('/login')
    return  Promise.reject(new Error('超时'))
}
```

被动处理 =》 超时  =》 code  => 10002

```js
if (code  => 10002) {
    // 删除token 
    store.dispatch('user/logout')
    router.push('/login')
}else {
    Message.error()
}
    return  Promise.reject(new Error('超时'))

```

localstorage 和 cookie的区别

```js
localstorage是纯前端的缓存
cookie 和后端的session是有关联的 cookie会存储sessionid => sessionid是保存当前用户会话的一个标识
```

## 上午总结

egg

express

koa

> 数据和算法 服务端编程  云函数

路由 

> 删除了一部分不用的文件  api/  views / 路由信息

新建了一部分 组件和路由

>  静态路由和动态路由

业务区分 =》 比如工资模块，有权限的人才可以看，没权限不让看

```js
export default {
    path: '/employess',
    name: 'employess', // 伏笔
    component: Layout,  // 静态路由区分开  /employees 
    children: [{
        path: '', // 因为我们想让该组件默认显示
        component: () => import('@/views/employees'),
        meta: {
        title: '员工管理', // 左侧菜单读取了title属性
        icon: 'peoples'
     }
    }]
}
```

静态和动态路由临时合并

```js
new Router({
    routes: [...contantRoutes, asyncRoutes]
})
```



组织架构头部

```vue
<el-row type="flex" justify="space-between">
  <el-col></el-col>
  <el-col>
      <el-row type="flex" justify="end"></el-row>
    </el-col>
</el-row>
```

## 下午总结

```js
比较两个数组相等
var a = [1,2,3]
var b = [3,2,1]
```

```js
var a = [{ name: '土豆' }, { name: '白菜' }]
var b = [{ name: '白菜' }, { name: '土豆' }]
```

递归

> 路由整理
>
> 静态路由  + 动态路由
>
> 临时合并  [...constantRoutes, ...asyncRoutes]

```js
export default {
    path: ''
    component: layout,
    children: [{
    path: '', // 什么都不写 表示默认二级路由
    component: () => import('地址')
     meta: {
         title: '',
          icon
     }
}]
}
```

> 组织业务

头部结构 

> el-row / el-col

树形组件

> el-tree  => data /props

静态结构

```vue
<el-tree>
   自定义内容
    <el-row slot-scope="{ data }"></el-row>
</el-tree>
```

封装抽提组件

```vue
<tree-tools>
 props: {
    treeNode: {}, // 显示节点数据
    isRoot: {}  // 是不是根节点
 }
```

获取组织架构数据

```js
created () {
    this.getDepartments()
},
methods: {
    getDepartments() {
     const result =	   await getDepartments()
     this.company = { name: result.companyName, manager: '负责人' }
      // result.depts
    }
}
```

递归算法

```js
function transListToTreeData (list, rootValue) {
    var arr = []
    list.forEach(item => {
        if(item.pid === rootValue) {
         const children =  transListToTreeData(list, item.id)
         if(children.length) {
             item.children = children
         }
            arr.push(item)
        }
    })
    return arr
}
```



